from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .forms import AccountLoginForm, AccountSignupForm

# Create your views here.


def account_login(request):
    login_view = "accounts/login.html"
    context = {}

    if request.method == "POST":
        form = AccountLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )

            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        form = AccountLoginForm()
    context = {
        "form": form,
    }

    return render(request, login_view, context)


def account_logout(request):
    logout(request)
    return redirect("login")


def account_signup(request):
    signup_view = "accounts/signup.html"
    context = {}
    # user = request.user

    if request.method == "POST":
        form = AccountSignupForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password != password_confirmation:
                context = {"error": "the passwords do not match"}
                return render(request, signup_view, context)
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            login(request, user)
            return redirect("list_projects")
    else:
        form = AccountSignupForm()
        context = {
            "form": form,
        }
        return render(request, signup_view, context)
