from django.shortcuts import render, redirect
from .models import Project
from .forms import ProjectForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": projects}
    return render(request, "projects/list.html", context)


def home(request):
    return redirect("list_projects")


@login_required
def show_projects(request, id):
    project_detail = Project.objects.get(id=id)
    tasks = project_detail.tasks.all()
    context = {"project_detail": project_detail, "tasks": tasks}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
        context = {"form": form}
        return render(request, "projects/create.html", context)
